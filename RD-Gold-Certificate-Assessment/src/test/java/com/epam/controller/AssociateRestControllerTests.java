package com.epam.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.epam.dto.AssociatesDto;
import com.epam.dto.BatchesDto;
import com.epam.dto.ExceptionResponse;
import com.epam.exception.AssociateException;
import com.epam.service.AssociatesService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(AssociateRestController.class)
class AssociateRestControllerTests {

	@Autowired
	MockMvc mockMvc;
	
	@MockBean
	AssociatesService associatesService;
	
	AssociatesDto associatesDto;
	BatchesDto batchesDto;
	
	@BeforeEach
	void setUp() {
		associatesDto=new AssociatesDto();
		associatesDto.setId(1);
		associatesDto.setCollege("Chitkara University");
		associatesDto.setGender("M");
		associatesDto.setName("Shagun");
		associatesDto.setEmail("shagun_garg@epam.com");
		associatesDto.setStatus("ACTIVE");
		batchesDto =new BatchesDto();
		batchesDto.setId(1);
		batchesDto.setName("RD-JAVA");
		batchesDto.setPractice("JAVA");
		batchesDto.setStartDate(LocalDate.parse("2023-05-26"));
		batchesDto.setEndDate(LocalDate.parse("2023-05-26"));
	}
	
	@Test
	void testAddAssociate() throws JsonProcessingException, Exception {
		when(associatesService.add(associatesDto)).thenReturn(associatesDto);
		mockMvc.perform(post("/rd/associates") .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(associatesDto)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(associatesDto.getId()))
                .andExpect(jsonPath("$.college", is(associatesDto.getCollege())))
                .andExpect(jsonPath("$.gender", is(associatesDto.getGender())))
                .andExpect(jsonPath("$.name", is(associatesDto.getName())))
                .andExpect(jsonPath("$.email", is(associatesDto.getEmail())))
                .andExpect(jsonPath("$.status", is(associatesDto.getStatus())))
                ;
		verify(associatesService).add(associatesDto);
	}
	@Test
	void testAddAssociateWithInvalidEmail() throws JsonProcessingException, Exception {
		associatesDto.setEmail("hn");
		when(associatesService.add(associatesDto)).thenReturn(associatesDto);
		mockMvc.perform(post("/rd/associates") .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(associatesDto)))
                .andExpect(status().isBadRequest())
                ;
	}
	
	@Test
	void testUpdateAssociate() throws JsonProcessingException, Exception {
		when(associatesService.update(associatesDto)).thenReturn(associatesDto);
		mockMvc.perform(put("/rd/associates") .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(associatesDto)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(associatesDto.getId()))
                .andExpect(jsonPath("$.college", is(associatesDto.getCollege())))
                .andExpect(jsonPath("$.gender", is(associatesDto.getGender())))
                .andExpect(jsonPath("$.name", is(associatesDto.getName())))
                .andExpect(jsonPath("$.email", is(associatesDto.getEmail())))
                .andExpect(jsonPath("$.status", is(associatesDto.getStatus())))
                ;
		verify(associatesService).update(associatesDto);
	}
	@Test
	void testGetByGenderAssociate() throws JsonProcessingException, Exception {
		when(associatesService.getByGender("M")).thenReturn(List.of(associatesDto));
		mockMvc.perform(get("/rd/associates/{gender}","M"))
                .andExpect(status().isOk());
		verify(associatesService).getByGender("M");
	}
	@Test
	void testDeleteAssociate() throws JsonProcessingException, Exception {
		doNothing().when(associatesService).delete(1);
		mockMvc.perform(delete("/rd/associates/{id}",1))
                .andExpect(status().isNoContent());
		verify(associatesService).delete(1);
	}
	
	@Test
	void testDeleteAssociateThrowsAssociateException() throws JsonProcessingException, Exception {
		doThrow(new AssociateException(" ")).when(associatesService).delete(1);
		mockMvc.perform(delete("/rd/associates/{id}",1))
                .andExpect(status().isBadRequest());
	}
	
	@Test
	void testDeleteAssociateThrowsMethodArgumentTypeMismatchException() throws JsonProcessingException, Exception {
		doThrow(MethodArgumentTypeMismatchException.class).when(associatesService).delete(1);
		mockMvc.perform(delete("/rd/associates/{id}",1))
                .andExpect(status().isBadRequest());
	}
	@Test
	void testDeleteAssociateThrowsDataIntegrityViolationException() throws JsonProcessingException, Exception {
		doThrow(DataIntegrityViolationException.class).when(associatesService).delete(1);
		mockMvc.perform(delete("/rd/associates/{id}",1))
                .andExpect(status().isBadRequest());
	}
	@Test
	void testDeleteAssociateThrowsRuntime() throws JsonProcessingException, Exception {
		doThrow(RuntimeException.class).when(associatesService).delete(1);
		mockMvc.perform(delete("/rd/associates/{id}",1))
                .andExpect(status().isInternalServerError());
	}
}
