package com.epam.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import com.epam.dto.AssociatesDto;
import com.epam.dto.BatchesDto;
import com.epam.exception.AssociateException;
import com.epam.model.Associates;
import com.epam.model.Batches;
import com.epam.repository.AssociateRepository;
import com.epam.service.AssociatesServiceImpl;

@ExtendWith(MockitoExtension.class)
public class AssociateServiceImplTest {

	@Mock
	AssociateRepository associateRepository;
	@Mock
	ModelMapper mapper;
	@InjectMocks
	AssociatesServiceImpl associatesService;

	AssociatesDto associatesDto;
	BatchesDto batchesDto;
	Associates associates;
	Batches batches;

	@BeforeEach
	void setUp() {
		associates = new Associates();
		associates.setId(1);
		associates.setCollege("Chitkara University");
		associates.setGender("M");
		associates.setName("Shagun");
		associates.setEmail("shagun_garg@epam.com");
		associates.setStatus("ACTIVE");
		batches = new Batches();
		batches.setId(1);
		batches.setName("RD-JAVA");
		batches.setPractice("JAVA");
		batches.setStartDate(LocalDate.parse("2023-05-26"));
		batches.setEndDate(LocalDate.parse("2023-05-26"));
		associates.setBatch(batches);
		associatesDto = new AssociatesDto();
		associatesDto.setId(1);
		associatesDto.setCollege("Chitkara University");
		associatesDto.setGender("M");
		associatesDto.setName("Shagun");
		associatesDto.setEmail("shagun_garg@epam.com");
		associatesDto.setStatus("ACTIVE");
		batchesDto = new BatchesDto();
		batchesDto.setId(1);
		batchesDto.setName("RD-JAVA");
		batchesDto.setPractice("JAVA");
		batchesDto.setStartDate(LocalDate.parse("2023-05-26"));
		batchesDto.setEndDate(LocalDate.parse("2023-05-26"));
		associatesDto.setBatchesDto(batchesDto);

		batches.getAssociateList();
		batches.getEndDate();
		batches.getId();
		batches.getName();
		batches.getStartDate();
		batches.getPractice();
		associates.getBatch();
		associates.getCollege();
		associates.getEmail();
		associates.getGender();
		associates.getId();
		associates.getName();
		associates.getStatus();

	}

	@Test
	void testAddAssociate() {
		when(mapper.map(associatesDto, Associates.class)).thenReturn(associates);
		when(mapper.map(associatesDto.getBatchesDto(), Batches.class)).thenReturn(batches);
		when(associateRepository.save(associates)).thenReturn(associates);
		when(mapper.map(associates,AssociatesDto.class)).thenReturn(associatesDto);
		when(mapper.map(associates.getBatch(), BatchesDto.class)).thenReturn(batchesDto);
		AssociatesDto addedDto= associatesService.add(associatesDto);
		assertEquals(associatesDto, addedDto);
		verify(mapper).map(associatesDto, Associates.class);
		verify(mapper).map(associatesDto.getBatchesDto(), Batches.class);
		verify(associateRepository).save(associates);
		verify(mapper).map(associates,AssociatesDto.class);
		verify(mapper).map(associates.getBatch(), BatchesDto.class);
	}

	@Test
	void testAddAssociateFails() {
		associatesDto.setGender("hg");
		assertThrows(AssociateException.class, () -> associatesService.add(associatesDto));
	}

	@Test
	void testUpdateAssociate()
	{
		when(associateRepository.findById(1)).thenReturn(Optional.of(associates));
		when(mapper.map(associatesDto.getBatchesDto(),Batches.class)).thenReturn(batches);
		AssociatesDto updatedDto = associatesService.update(associatesDto);
		assertEquals(associatesDto, updatedDto);
		verify(associateRepository).findById(1);
		verify(mapper).map(associatesDto.getBatchesDto(),Batches.class);
	}

	@Test
	void testUpdateAssociateFails() {
		associatesDto.setGender("hg");
		assertThrows(AssociateException.class, () -> associatesService.update(associatesDto));
	}

	@Test
	void testUpdateAssociateNotPresentInDatabase()
	{
		when(associateRepository.findById(1)).thenReturn(Optional.empty());
		assertThrows(AssociateException.class,()->associatesService.update(associatesDto));
	}

	@Test
	void testDelete() {
		doNothing().when(associateRepository).deleteById(1);
		associatesService.delete(1);
		verify(associateRepository).deleteById(1);
	}

	@Test
	void testGetByGender() {
		when(associateRepository.findByGender("M")).thenReturn(List.of(associates));
		when(mapper.map(associates, AssociatesDto.class)).thenReturn(associatesDto);
		when(mapper.map(associates.getBatch(), BatchesDto.class)).thenReturn(batchesDto);
		List<AssociatesDto> associatesList=associatesService.getByGender("M");
		assertEquals(associatesList.get(0), associatesDto);
	}

	@Test
	void equals_sameInstance_true() {
		assertTrue(associatesDto.equals(associatesDto));
	}

	@Test
	void equals_differentClasses_false() {
		assertFalse(associatesDto.equals(new Object()));
	}

	@Test
	void equals_differentTypes_false() {
		assertFalse(associatesDto.equals(batches));
	}

	@Test
	void hashCode_nullFields_sameHashCodes() {
		AssociatesDto associate2 = new AssociatesDto();
		associate2.setId(1);
		associate2.setCollege("Chitkara University");
		associate2.setGender("M");
		associate2.setName("Shagun");
		associate2.setEmail("shagun_garg@epam.com");
		associate2.setStatus("ACTIVE");
		associate2.setBatchesDto(batchesDto);
		assertEquals(associatesDto.hashCode(), associate2.hashCode());
	}

	@Test
	void hashCode_nullId_differentHashCodes() {
		AssociatesDto associate2 = new AssociatesDto();
		associate2.setId(0);
		associate2.setCollege("Chitkara University");
		associate2.setGender("M");
		associate2.setName("Shagun");
		associate2.setEmail("shagun_garg@epam.com");
		associate2.setStatus("ACTIVE");

		assertNotEquals(associatesDto.hashCode(), associate2.hashCode());
	}

}
