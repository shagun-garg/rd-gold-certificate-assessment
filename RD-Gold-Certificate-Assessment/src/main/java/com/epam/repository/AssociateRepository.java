package com.epam.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.model.Associates;

public interface AssociateRepository extends JpaRepository<Associates, Integer> {

	List<Associates> findByGender(String gender);
	
}
