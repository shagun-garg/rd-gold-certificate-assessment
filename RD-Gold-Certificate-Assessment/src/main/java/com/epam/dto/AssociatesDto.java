package com.epam.dto;

import com.epam.model.Batches;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class AssociatesDto {
	int id;
	@NotBlank(message = "name cannot be blank")
	String name;
	@NotBlank(message = "email cannot be blank")
	@Email(message = "enter a valid email address")
	String email;
	@NotBlank(message = "gender cannot be blank")
	String gender;
	@NotBlank(message = "college cannot be blank")
	String college;
	@NotBlank(message = "status cannot be blank")
	String status;
	BatchesDto batchesDto;

}
