package com.epam.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ExceptionResponse {

	String timeStamp;
	String status;
	String error;
	String path;
}
