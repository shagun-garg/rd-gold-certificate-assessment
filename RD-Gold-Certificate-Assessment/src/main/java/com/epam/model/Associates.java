package com.epam.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "assosiates")
public class Associates {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "assosiate_id", nullable = false, unique = true)
	private int id;
	@Column(name = "name", nullable = false)
	private String name;
	@Column(name = "email", nullable = false, unique = true)
	private String email;
	@Column(name = "gender", nullable = false)
	private String gender;
	@Column(name = "college", nullable = false)
	private String college;
	@Column(name = "status", nullable = false)
	private String status;
	@ManyToOne
	@JoinColumn(name = "batch_id", referencedColumnName = "batch_id")
	private Batches batch;

	public Batches getBatch() {
		return batch;
	}

	public void setBatch(Batches batch) {
		batch.getAssociateList().add(this);
		this.batch = batch;
	}

}
