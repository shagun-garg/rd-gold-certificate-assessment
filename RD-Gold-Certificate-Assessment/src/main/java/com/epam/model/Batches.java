package com.epam.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name="batches")
public class Batches {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name="batch_id",nullable = false)
	private int id;
	@Column(name="name",nullable = false,unique = true)
	private String name;
	@Column(name="practice",nullable = false,unique = true)
	private String practice;
	@Column(name="start_date",nullable = false)
	private LocalDate startDate;
	@Column(name="end_date",nullable = false)
	private LocalDate endDate;
	@OneToMany(mappedBy = "batch", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Associates> associateList=new ArrayList<>();
}
